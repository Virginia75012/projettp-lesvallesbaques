<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Form\RegistrationType;
use App\Repository\AppRoleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class PasswordController extends AbstractController
{
    /**
    * @Route("/oubli-mot-de-passe", name="app_forgotten_password", methods="GET|POST")
    */
    public function forgottenPassword(Request $request, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer, TokenGeneratorInterface $tokenGenerator): Response
    {
        if ($request->isMethod('POST')) {

            $email = $request->request->get('email');

            $entityManager = $this->getDoctrine()->getManager();
            $user = $entityManager->getRepository(User::class)->findOneByEmail($email);

            if ($user === null) {

                $this->addFlash(
                    'danger',
                    'Email Inconnu, recommence !'
                );

                return $this->redirectToRoute('app_forgotten_password');
            }

            $token = $tokenGenerator->generateToken();

            try {
                $user->setResetToken($token);
                $entityManager->flush();
            } catch (\Exception $e) {
                $this->addFlash('warning', $e->getMessage());
                return $this->redirectToRoute('home');
            }

            $url = $this->generateUrl('app_reset_password', array('token' => $token), UrlGeneratorInterface::ABSOLUTE_URL);

            $message = (new \Swift_Message('Oubli de mot de passe - Réinisialisation'))
                ->setFrom(array('vivioclock@gmail.com'=> 'Les VallesBaques'))
                ->setTo($user->getEmail())
                ->setBody(
                $this->renderView(
                    'security/emails/resetPasswordMail.html.twig',
                    [
                        'user'=>$user,
                        'url'=>$url
                    ]
                ),
                    'text/html'
                );
            $mailer->send($message);

            $this->addFlash(
                'notice',
                'Mail envoyé, tu vas pouvoir te connecter à nouveau !'
            );

            return $this->redirectToRoute('security_login');
        }

        return $this->render('security/forgottenPassword.html.twig');
    }

    /**
     * @Route("/reinitialiser-mot-de-passe/{token}", name="app_reset_password")
     */
    public function resetPassword(Request $request, string $token, UserPasswordEncoderInterface $passwordEncoder)
    {
        //Reset avec le mail envoyé
        if ($request->isMethod('POST')) {

            $entityManager = $this->getDoctrine()->getManager();

            $user = $entityManager->getRepository(User::class)->findOneByResetToken($token);

            if ($user === null) {

                $this->addFlash(
                    'danger',
                    'Mot de passe non reconnu'
                );

                return $this->redirectToRoute('home');
            }

            $user->setResetToken(null);
            $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('password')));
            $entityManager->flush();

            $this->addFlash(
                'notice',
                'Mot de passe mis à jour !'
            );

            return $this->redirectToRoute('security_login');
        }else {

            return $this->render('security/resetPassword.html.twig', ['token' => $token]);
        }
    }
}