<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Form\AccountType;
use App\Service\FileUploader;
use App\Form\ChangePasswordType;
use App\Form\Model\ChangePassword;
use App\Repository\CrewRepository;
use App\Repository\UserRepository;
use App\Repository\QuizzRepository;
use App\Repository\UserCrewRepository;
use App\Repository\StatisticRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/membres/public/{id}", name="list_users_free" ,defaults={"id"=0})
     * @IsGranted("ROLE_USER")
     */
    public function usersList(UserRepository $ur, CrewRepository $cr, $id)
    {
        $user = $this->getUser();
        $users = $ur->findAll();

        ((0 === $id)? $crew = null : $crew = $cr->findOneById($id));

        return $this->render('user/list.html.twig', [
            'users' => $users,
            'crew' => $crew,
        ]);
    }
}