<?php

namespace App\Form;

use App\Entity\User;
use App\Form\ApplicationType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class AccountType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, $this->getConfiguration("Email", "Ton adresse email"))
            ->add('userName', TextType::class, $this->getConfiguration("Prénom ou pseudo", "Ton prénom ou pseudo"))
            ->add('avatar', FileType::class, [
                'data_class' => null,
                'required'=>null
            ])
            ->add('presentation', null, [
                'label'=> 'Présentation',
                'attr'=> [
                    'placeholder'=> "Décris-toi en quelques mots"
                ]
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
