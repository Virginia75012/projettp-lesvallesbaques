<?php

namespace App\Form;

use App\Entity\Crew;
use App\Form\ApplicationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class NewCrewType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, $this->getConfiguration("Nom du Groupe", "Tape le nom de ton groupe"))
            ->add('description', TextareaType::class, $this->getConfiguration("Présenation du groupe", "Description sur le groupe..."), [
                'required' => false,
                'data_class' => null,
            ])
            ->add('avatar', null, [
                'label'=>'Image',
                'data_class' => null,
                'required' => false,
                'help' => 'Si tu es pressé(e), pas de souci tu pourras l\'ajouter plus tard dans ton profil'
            ])
            ->add('isPrivate', ChoiceType::class, [
                'label' => 'Ton groupe est privé ou public ? Par défaut, il sera visible pour tout le monde, alors vérfie bien si tu veux opter pour un groupe privé',
                'choices' => [
                    'Privé' => 1,
                    'Public'=> 0,
                ],
                'expanded' => true,
                'help' => '"Privé" = Tu seras obligé(e) d\'inviter tous les nouveaux membres - ouvert = toutes les personnes peuvent s\'ajouter à ton groupe',
                'required'=> true,
                'data'=> false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Crew::class,
        ]);
    }
}
