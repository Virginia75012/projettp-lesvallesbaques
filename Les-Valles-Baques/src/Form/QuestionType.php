<?php

namespace App\Form;

use App\Form\QuizzType;
use App\Entity\Question;
use App\Form\ApplicationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class QuestionType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('body', TextareaType::class, [
                'label'=>'Question',
                'help' => 'Sois précis(e)',
                'attr'=>[
                    'placeholder'=>'Ecris ta Question'
                ]
            ])
            ->add('imageFile', VichImageType::class, [
                'label'=>'Image*',
                'help'=>'L\'image n\'est pas forcément obligatoire',
                'required' => false,
                'attr'=>[
                    'placeholder'=>'Ajoute une image'
                ]
            ])
            ->add('level', null, [
                'label'=>'Niveau de la Question',
                'expanded' => true,
            ])
            ->add('prop1', TextType::class, $this->getConfiguration("Bonne Réponse", "Ecris la bonne réponse"))
            ->add('prop2', TextType::class, $this->getConfiguration("Fausse réponse n°1", "Ecris la première fausse réponse"))
            ->add('prop3', TextType::class, $this->getConfiguration("Fausse réponse n°2", "Ecris la deuxième fausse réponse"))
            ->add('prop4', TextType::class, $this->getConfiguration("Fausse réponse n°3", "Ecris la troisième fausse réponse"))
            ->add('anecdote', null, $this->getConfiguration("Anecdote", "Ecris une anecdote en lien avec la question"), [
                'required' => false
            ])
            ->add('source', UrlType::class, [
                'required' => false,
                'label' => 'Source',
                'help' => 'Pense à mettre un lien vers un article au cas où on voudrait en savoir plus',
                'attr'=>[
                    'placeholder'=>'Insère un lien de ta source'
                ]
            ])
            ->setAttributes([
            'novalidate'=>'novalidate',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Question::class,
            'attr' => ['id' => 'nextQuestion',
            'novalidate' => 'novalidate',
            ],
            'method'=> 'POST'
        ]);
    }
}
