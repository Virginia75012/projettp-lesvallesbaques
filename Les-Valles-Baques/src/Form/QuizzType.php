<?php

namespace App\Form;

use App\Entity\Quizz;
use App\Form\CrewType;
use App\Form\QuestionType;
use App\Form\ApplicationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class QuizzType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, $this->getConfiguration("Titre du Quiz", "Tape le titre du Quiz"))
            ->add('description', null, $this->getConfiguration("Description du Quiz", "Tape un petit descriptif du Quiz"), [
                'required' => false
            ])

            ->add('category', null, $this->getConfiguration("Choisis une Catégorie dans la liste", ""))
            //? on pourrait le calculé a partir des difficultés des questions ?
            ->add('level', null, $this->getConfiguration("Choisis le niveau de difficulté du Quiz", ""))
            /*->add('IsPrivate', ChoiceType::class, [
                'choices'  =>[

                    'privée'=> 1,
                    'public'=> 0
                ],
                'expanded' => true,
                'multiple' => true,
                'label'=>'Privé',
                'help'=>'Si tu coches cette option, le Quizz ne sera visible que dans ton groupe actuel'
            ])*/
            ->setAttributes([
            'novalidate'=>'novalidate',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Quizz::class,
        ]);
    }
}
