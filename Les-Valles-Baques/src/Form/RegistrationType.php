<?php

namespace App\Form;

use App\Entity\User;
use App\Form\ApplicationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class RegistrationType extends ApplicationType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, $this->getConfiguration("Email", "Ton adresse email"))
            ->add('userName', TextType::class, $this->getConfiguration("Prénom ou pseudo", "Ton prénom ou pseudo"))
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe doivent correspondre',
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options'  => [
                    'label' => 'Mot de passe *',
                    'help' => '8 caratères, dont une lettre et un chiffre, c\'est long mais c\'est pour toi',
                    'attr'=> [
                        'placeholder'=> 'Mot de passe ...'
                    ]
                ],
                'second_options' => [
                    'label' => 'Confirmation du mot de passe *',
                    'attr'=> [
                        'placeholder'=> 'Veille à confirmer ton mot de passe'
                    ]
                ],
            ])
            ->add('avatarFile', VichImageType::class, [
                'label'=> 'Photo de profil',
                'required' => false,
                'help' => 'Si tu es pressé(e), pas de souci tu pourras l\'ajouter plus tard dans ton profil',
            ])
            ->add('presentation', null, [
                'label'=> 'Présentation',
                'required' => false,
                'help' => 'Si tu es pressé(e), pas de souci tu pourras le remplir plus tard dans ton profil',
                'attr'=> [
                    'placeholder'=> "Décris-toi en quelques mots"
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
