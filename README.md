Projet Les Valles Baques
==

## Projet fin de formation chez O'Clock (03/10/2018)
### Développeurs Junior : Vincent Delacour et Virginia Atenza

Nous avons travaillé initilament à 2 durant le mois de septembre 2018 sur ce repository :

https://github.com/O-clock-Journey/Projet-VallesBaques


J'ai crée ici ce repository pour travailler individuellement sur la suite du projet afin de préparer mon Titre Professionnel.

---

### Synthèse

J'ai voulu ici refactoriser le code, mieux organiser le SCSS, les templates Twig, ajouter quelques fonctionnalités que nous n'avions pas mis en place.

Aussi quelques fonctionnalités n'étaient pas au point lors de la présentation du projet en public sur Twitch le 3 octobre 2019, malheureusement par manque de temps (1 mois) étant uniquement un binôme de développeurs bien motivés.

Il fallait retravailler sur quelques point essentiels :
- le reset du mot de passe lorsque celui-ci est oublié et/ou perdu depuis la page connexion
- pouvoir modifier ses informations personnelles de son compte utilisateur sans pour autant être déconnecté
- modifier son mot de passe depuis son compte utilisateur
- Swiftmailer pour un utilisateur inviter dans un groupe
- quelques addFlash (alertes) qu'il nous manquait à mettre en place afin d'avertir l'utilisateut si ses actions se sont bien déroulés ou pas
- pop-up suite à la suppression d'un quiz en cours ou d'un groupe

### D'autres fonctionnalités seraient à venir

Il manque encore beaucoup de chose afin de rendre cette application encore plus riche en fonctionnalités :
- pouvoir se connecter avec Facebook et autres réseaux sociaux
- mise en place de l'envoi en Ajax de nos formulaires de questions
- mise en place de messagerie entre les membres du groupes et/ou du site
- créer un Service Symfony pour l'authentification des rôles dans le groupe
- lancer des défis aux autres membres
- créer un système de trophées et/ou de badge pour voir l'évolution
- permettre à un utilisateur de s'incrire dans un groupe sans y être invité

Voici donc une première liste de fonctionnalités que nous avons dans un premier temps repéré. Il y en a certainement d'autres ;-)